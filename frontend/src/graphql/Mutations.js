import { gql } from "@apollo/client";

export const CREATE_USER_MUTATION = gql`
    mutation {
        createUser(
        createUser: {
            email: $email
            lastName: $lastName
            firstName: $firstName
            userId: $userId
            created_at: $created_at
            updated_at: $updated_at
            password: $password
        }
        ) {
        access_token
        }
    }


`