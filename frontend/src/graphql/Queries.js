import { gql } from "@apollo/client";

export const getMemo = gql`
    query {
        getMemo(memoIds: "939f8311-295b-4475-864b-a9750281681a") {
          title
          content
          userId
        }
      }`

// export const getUser = gql`

// `