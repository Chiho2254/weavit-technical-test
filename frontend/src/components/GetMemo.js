import React, { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { getMemo } from "../graphql/Queries";

function GetMemos() {
    const { error, loading, data } = useQuery(getMemo);
    const [Memos, setMemos] = useState([]);
    useEffect(() => {
        if (data) {
            setMemos(data.getMemo);
        }
    }, [data]);
    console.log(Memos);
    return (
        <div>
            I am a memo
    </div>
    );
}

export default GetMemos;