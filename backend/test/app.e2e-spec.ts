import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppController } from '../src/app.controller';
import { AppService } from '../src/app.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  // describe('Auth', () => {
  //   describe('POST /auth/register', () => {
  //     it('should validate the request', () => {
  //       return request(app.getHttpServer()).post('/auth/register').set('Accept', 'application/json').send({
  //         email: 'testing@testing.come',
  //         firstName: 'Chiho'
  //       }).expect(400).expect(res => {
  //         console.log(res.body);
  //       })
  //     })
  //   })
  // })

  describe('Auth', () => {
    describe('POST /auth/register', () => {
      it('should create user', () => {
        return request(app.getHttpServer()).post('/auth/register').set('Accept', 'application/json').send({
          email: 'testing@testing.com',
          firstName: 'Chiho',
          lastName: 'Chang',
          password: 'weavitTest'
        }).expect(200).expect(res => {
          console.log(res.body);
        })
      })
    })
  })
});
