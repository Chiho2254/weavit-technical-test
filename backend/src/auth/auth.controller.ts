import { Controller, Post, Body, UseGuards, Request, Get } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { LocalAuthGuard } from './local-auth.guard';
import { GqlAuthGuard } from './jwt-auth-guard';
import { AuthService } from './auth.service';
import { CreateUserInput } from '../user/dto/input/create-user.input';

@Controller('auth')
export class AuthController {
    constructor(private readonly userService: UserService, private readonly authService: AuthService){}

    @Post('register')
    async postRegister(@Body() user: CreateUserInput) {
        const res = await this.userService.createUser(user)

        return await this.authService.createToken(res);
    }
    
    @UseGuards(LocalAuthGuard)
    @Post('login')
    async postLogin(@Request() request) {
        return await this.authService.createToken(request.user);
    }


    @UseGuards(GqlAuthGuard)
    @Get('user')
    async getUser(@Request() request) {
        return request.user.properties;
    }

}
