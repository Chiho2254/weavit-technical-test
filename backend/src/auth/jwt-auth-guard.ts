import { Injectable, ExecutionContext, CanActivate, BadRequestException, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { GqlExecutionContext } from "@nestjs/graphql";
import { AuthService } from "./auth.service";


@Injectable()
export class GqlAuthGuard implements CanActivate {
    constructor(private readonly authService: AuthService) { }

    getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        return ctx.getContext().req;
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = this.getRequest(context);
        const authHeader = req.headers.authorization as string;

        if (!authHeader) {
            throw new BadRequestException('Authorization header not found.');
        }
        const [type, token] = authHeader.split(' ');
        if (type !== 'Bearer') {
            throw new BadRequestException(`Authentication type \'Bearer\' required. Found \'${type}\'`);
        }
        const user = await this.authService.ValidateToken(token);

        if (user) {
            req.user = user;
            // console.log(user);
            return user;
        }
        throw new UnauthorizedException('Token not valid');
    }
}