import { ObjectType, Field } from "@nestjs/graphql";


@ObjectType()
export class Jwt {
    @Field()
    access_token: string;
}