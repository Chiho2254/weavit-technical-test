import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { EncryptionService } from '../encryption/encryption.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/models/user';

@Injectable()
export class AuthService {

    constructor(private readonly userService: UserService, private readonly encryptionService: EncryptionService, private readonly jwtService: JwtService) { }

    async validateUser(email: string, password: string) {
        const user = await this.userService.findUserByEmail(email);
        if (user && await this.encryptionService.compare(password, (<Record<string, any>>user).password)) {

            return user;
        }

        return null;
    }

    async createToken(user: User) {
        const { userId, email, firstName, lastName } = <Record<string, any>>user;

        return {
            access_token: this.jwtService.sign({
                userId: userId,
                email, firstName, lastName
            })
        }
    }

    async ValidateToken(token: string) {
        try {
            // console.log(await this.jwtService.verify(token));
            return await this.jwtService.verify(token);
        } catch (error) {
            return error.name;
        }
    }
}
