import { Injectable, Query } from '@nestjs/common';
import { Neo4jService } from '../neo4j/neo4j.service';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { CreateMemoInput } from '../memo/dto/input/create-memo.input';
import { Memo } from './models/memo';
import { v4 as uuidv4} from 'uuid';
import { User } from '../user/models/user';


@Injectable()
export class MemoService {
    constructor(private readonly neo4jService: Neo4jService, private readonly jwtService: JwtService) { }




    async createMemo(createMemoInput: CreateMemoInput, user: User): Promise<Memo> {

        // const getUserInfo = this.jwtService.decode('')
        const memoInfo: Memo = {
            memoId: uuidv4(),
            created_at: new Date(),
            updated_at: new Date(),
            userId: user.userId,
            firstName: user.firstName,
            lastName: user.lastName,
            ...createMemoInput
        }
        const res = await this.neo4jService.write(`CREATE (m:Memo) SET m += $properties, m.id = randomUUID(), m.created_at = datetime(), m.updated_at = datetime() RETURN m`, {
            properties: {
                ...memoInfo
            }
        })

        return res.records[0].get('m').properties;

    }

    async getAllMemo() {
        const result = await this.neo4jService.read(`
            MATCH (m:Memo) RETURN m
        `)
        let memos = []
        for(let i = 0; i < result.records.length; i++){
             memos.push(result.records[i].get('m').properties);
        }
        return memos;
        // return result.records[0].get('m').properties;
    }

}
