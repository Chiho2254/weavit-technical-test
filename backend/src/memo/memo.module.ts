import { Module } from '@nestjs/common';
import { MemoService } from './memo.service';
import { MemoController } from './memo.controller';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from '../user/user.module';
import { EncryptionModule } from '../encryption/encryption.module';
import { MemoResolver } from './memo.resolver';
import { AuthService } from '../auth/auth.service';

@Module({
    imports: [
        JwtModule.registerAsync({
            imports: [ConfigModule,],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                secret: configService.get<string>('JWT_SECRET'),
                signOptions: {
                    expiresIn: configService.get<string>('JWT_EXPIRES_IN', '30d'),
                },
            })
            // secret: jwtConstants.secret,
            // signOptions: { expiresIn: '30d' },
        }), UserModule, EncryptionModule],
    providers: [MemoService, MemoResolver, AuthService],
    controllers: [MemoController]
})
export class MemoModule {}
