import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsUUID } from 'class-validator';

@InputType()
export class DeleteMemoInput {
 
    @Field()
    @IsNotEmpty()
    @IsUUID()
    userId: string;


    @Field()
    @IsNotEmpty()
    @IsUUID()
    memoId: string;
}