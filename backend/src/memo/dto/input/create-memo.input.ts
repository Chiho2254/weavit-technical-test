import { IsNotEmpty, IsUUID, IsOptional } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateMemoInput {

    @Field()
    @IsNotEmpty()
    title: string;

    @Field()
    @IsNotEmpty()
    content: string;

    @Field()
    @IsNotEmpty()
    @IsUUID()
    userId: string;

    @Field()
    @IsOptional()
    created_at?: Date;

    @Field()
    @IsOptional()
    updated_at?: Date;

}