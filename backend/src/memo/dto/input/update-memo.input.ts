import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsUUID } from 'class-validator';

@InputType()
export class UpdateMemoInput {

    @Field()
    @IsNotEmpty()
    @IsUUID()
    userId: string;

    @Field()
    @IsNotEmpty()
    content: string;

    @Field()
    @IsNotEmpty()
    titile: string;

    @Field()
    @IsNotEmpty()
    updated_at: Date;
}