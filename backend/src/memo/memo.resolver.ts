import { Query, Resolver, Mutation, Args } from '@nestjs/graphql';
import { MemoService } from './memo.service';
import { Memo } from './models/memo';
import { GetMemosArgs } from './dto/args/get-memos.args';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/jwt-auth-guard';
import { JwtService } from '@nestjs/jwt';
import { CurrentUser } from '../user/user.decorator'
import { CreateMemoInput } from './dto/input/create-memo.input';

@Resolver(() => Memo)
export class MemoResolver {
    constructor(private readonly memoService: MemoService, private readonly jwtService: JwtService){}


    // @UseGuards(GqlAuthGuard)
    @Query(()=> Memo)
    async getMemo() {
        // console.log(CurrentUser)
        const memos = await this.memoService.getAllMemo()
        console.log(memos);
        return memos;
    }

    @UseGuards(GqlAuthGuard)
    @Mutation(() => Memo)
    async createMemo(@CurrentUser()user, @Args('createMemo') memo: CreateMemoInput) {
        // console.log(user);
        const res = await this.memoService.createMemo(memo, user)
        return res;

    }

}