import { ObjectType, Field } from "@nestjs/graphql";

@ObjectType()
export class Memo {
    constructor() { }
    @Field()
    memoId: string;
    @Field()
    userId: string;
    @Field()
    title: string;
    @Field()
    content: string;
    @Field()
    firstName: string;
    @Field()
    lastName: string;
    @Field()
    created_at?: Date;
    @Field()
    updated_at?: Date;
}