
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class CreateUserInput {
    userId: string;
    email: string;
    password: string;
    lastName: string;
    firstName: string;
    created_at: DateTime;
    updated_at: DateTime;
}

export class UpdateUserInput {
    userId: string;
    updated_at: DateTime;
}

export class DeleteUserInput {
    userId: string;
}

export class CreateMemoInput {
    title: string;
    content: string;
    userId: string;
    created_at: DateTime;
    updated_at: DateTime;
}

export class User {
    userId: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    created_at: DateTime;
    updated_at: DateTime;
}

export class Jwt {
    access_token: string;
}

export class Memo {
    memoId: string;
    userId: string;
    title: string;
    content: string;
    firstName: string;
    lastName: string;
    created_at: DateTime;
    updated_at: DateTime;
}

export abstract class IQuery {
    abstract user(userId: string): User | Promise<User>;

    abstract users(userIds: string[]): User[] | Promise<User[]>;

    abstract getMemo(): Memo | Promise<Memo>;
}

export abstract class IMutation {
    abstract createUser(createUser: CreateUserInput): Jwt | Promise<Jwt>;

    abstract updateUser(updateUser: UpdateUserInput): User | Promise<User>;

    abstract deleteUser(deleteUser: DeleteUserInput): User | Promise<User>;

    abstract createMemo(createMemo: CreateMemoInput): Memo | Promise<Memo>;
}

export type DateTime = any;
