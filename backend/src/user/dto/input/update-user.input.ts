import { InputType, Field } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class UpdateUserInput {

    @Field()
    @IsNotEmpty()
    userId: string;

    @Field()
    @IsNotEmpty()
    updated_at: Date;
}