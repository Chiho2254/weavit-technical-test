import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsUUID } from 'class-validator';

@InputType()
export class DeleteUserInput {
 
    @Field()
    @IsNotEmpty()
    @IsUUID()
    userId: string;
}