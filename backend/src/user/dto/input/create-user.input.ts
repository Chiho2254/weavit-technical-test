import { InputType, Field } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsOptional, IsUUID } from 'class-validator';

@InputType()
export class CreateUserInput {

    @Field()
    @IsUUID()
    userId?: string;

    @Field()
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @Field()
    @IsNotEmpty()
    password: string;

    @Field()
    @IsNotEmpty()
    lastName: string;

    @Field()
    @IsNotEmpty()
    firstName: string;

    @Field()
    @IsOptional()
    created_at?: Date;

    @Field()
    @IsOptional()
    updated_at?: Date;

}